# DetectEat_ML

This project detects foods on plate, and classifies it.
Well trained model.
Used darknet features and yolo2 for object detection.

# detecteat_ml

Contains the project with all his aspects.
Go inside to see all code and used data.

# How To Run

You will have to run it on linux OS or bashes - like cygwin (recommended).

The running is by the Predict script - takes all the results from the machine,
and takes the best one.
Pay attention the the result is gathered from the terminal and is written in a 
log file.

Have fun!