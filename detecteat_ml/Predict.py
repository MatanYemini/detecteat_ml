import glob, os
import subprocess
from subprocess import Popen, PIPE

def get_max():

    # Current directory
    current_dir = os.path.dirname(os.path.abspath(__file__))
    log = "log.txt"

    with open(current_dir + "//" + log) as f:
        content = f.read().splitlines()

    i = 0
    detect_food = []

    for line in content:
        i += 1
        if line.find('Predicted in') != -1:
            detect_food = content[i:]

    i = 0
    food_dic = {}
    key = ""

    for curr_food in detect_food:
        i += 1
        if i % 2 != 0:
            key = str(curr_food).lower()
            if not key in food_dic.keys():
                food_dic[key] = 0
        else:
            val, per = str(curr_food).split("%")
            pre, val = str(val).split(": ")
            val = int(val, base=10)
            if key in food_dic.keys():
                if food_dic[key] < val:
                    food_dic[key] = val

    key_max = max(food_dic.keys(), key=(lambda k: food_dic[k]))
    print(key_max)


def go_to_folder():
    command = "cd ..//..//..//..//"
    os.chdir(r"C:\cygwin64\bin")
    cmd = ["bash", "-c", "" + command + ";"]
    ret = subprocess.call(cmd)
    darknet_path = "C://100foods//darknet"
    command = "cd {}".format(darknet_path)
    os.chdir(r"C:\cygwin64\bin")
    cmd = ["bash", "-c", "{};".format(command)]
    ret = subprocess.call(cmd)

    return ret

def predict(image = "My_amazing_food3"):
    #ret = go_to_folder()
    path = os.path.dirname(os.path.abspath(__file__)) + "\\"
    path += image
    command = build_command(path)
    print("The image path is: {} ".format(path))
    print("the command is: {} ".format(command))
    os.chdir(r"C:\cygwin64\bin")
    cmd = ["bash", "-c", "{}; exit;".format(command)]
    ret = subprocess.call(cmd)
    print(get_max())

def build_command(path):
    command = ".\\darknet detector test data\\food100\\food100.data data\\food100\\yolov2-food100.cfg " \
              "backup\\yolov2-food100_100.weights " + path + " 2<{}1 | tee -a log.txt".format("$")

    return command

def main():
    print("All Right lets try")
    predict()

if __name__ == '__main__':
    main()


 #./darknet detector test data/food100/food100.data data/food100/yolov2-food100.cfg backup/yolov2-food100_100.weights My_amazing_food.jpg 2<&1 | tee -a log.txt;


